const express = require('express')
const cors = require('cors')
const { tasks } = require('./data')

console.clear()
console.log('start')

const app = express()
app.use(cors())


app.get('/', (req, res) => {
	res.write("<h1>Doker test</h1>")
	res.send()
})

app.get('/tasks', (req, res) => {
	res.json(tasks)
})

app.get('/tasks/:id', (req, res) => {
	const id = req.params.id
	let task = tasks[id - 1]
	if (task) {
		res.json(tasks[id - 1])
	} else {
		res.status(404).send(`task with id ${id} not found`)
	}
})


const port = process.env.PORT || 3000

app.listen(port, () => console.log(`listening on localhost:${port}`))