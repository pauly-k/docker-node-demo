# setup

A dockeraized node application for demonstation purposes.

## remove docker container and image

```bash
docker container stop docker_node
docker container rm docker_node
docker image rm docker-node-test_app
docker-compose up
```

## down

```bash
docker-compose down
```
