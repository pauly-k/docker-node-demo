
const tasks = [
	{ id: 1, title: 'learn docker compose' },
	{ id: 2, title: 'run docker image on digital ocean' },
	{ id: 3, title: 'run swapi graphql service on digitial ociean' },
	{ id: 4, title: 'get type graphql working for timetrack' },
	{ id: 5, title: 'Data modeling for ford -- autoline middleware' },
	{ id: 6, title: 'Create svelte based app to store interface mock data and related json conversion templates' },
]

module.exports = { tasks }